package Libreria;

import static Libreria.LibreriaPH.dautor;

public class Libro extends LibreriaPH{//CLASE HIJA
    
    protected double derechos;
    protected double total;
    protected double total1;
    protected double total2;
    
    
    public Libro(double costo, double iva, double dautor, double costo_total,double total1, double derechos,double total2, double reconocimiento, double total, double utilidad){//CONSTRUCTOR
         super(costo, iva, dautor, costo_total,reconocimiento,utilidad);
         this.derechos= derechos;
         this.total=total;
         this.total1=total1;
         this.total2=total2;
     }
     
     
     double venderlibro( double costo){
        total=(costo*dautor);
        total1=costo*iva;
        total2=costo*utilidad;
        costo_total=costo+total+total1+total2;
        System.out.print("El total de la compra es: "+costo_total);
        return costo_total;
    }
     double comprarlibro(double costo){
    total=costo*reconocimiento;
    costo_total=costo+total;
    System.out.print("El total de la venta es: "+costo_total);
    return costo_total;
}
}
