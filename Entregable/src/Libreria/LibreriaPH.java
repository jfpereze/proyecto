package Libreria;
import java.util.Scanner;
public class LibreriaPH {
    
    Scanner lea = new Scanner(System.in);
    static protected double costo;
    static protected double iva;
    static protected double dautor;
    static protected double costo_total;
    static protected double reconocimiento;
    static protected double utilidad;
    
    public LibreriaPH(double costo, double iva, double dautor, double costo_total, double reconocimiento, double utilidad) {
        this.costo = costo;
        this.iva = iva;
        this.dautor = dautor;
        this.costo_total = costo_total;
        this.reconocimiento=reconocimiento;
        this.utilidad=utilidad;
    }

    public void leercosto(){
        System.out.print("Digite el costo del producto: ");
        costo=lea.nextDouble();
    }

    
    public void calculariva(){
        double ivap;
        System.out.print("Digite valor del porcentaje del iva: ");
        ivap=lea.nextDouble();
        iva=ivap/100;
    }
    
    public void calcularreconocimiento(){
        double rec;
        System.out.print("Digite el valor del reconocimiento: ");
        rec=lea.nextDouble();
        reconocimiento=rec/100;
    }
    
    public void calculardautor(){
        double autor;
        System.out.print("Digite el valor de Derechos de Autor: ");
        autor=lea.nextDouble();
        dautor=autor/100;
    }
    
    public void calcularutilidad(){
        double ult;
        System.out.print("Digitar el valor de utilidad: ");
        ult=lea.nextDouble();
        utilidad=ult/100;
    }
}
