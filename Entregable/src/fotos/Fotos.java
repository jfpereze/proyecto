
package fotos;

import java.awt.Graphics;//Importa la clase Graphics que permite dibujar cualquier figura o mostrar cualquier imagen
import javax.swing.ImageIcon;//La interfaz de iconos que pinta iconos de imágenes. Las imágenes que se crean a partir de una URL, un nombre de archivo 

public class Fotos {

    //Se crearan los métodos mediante ImageIcon para poder traer a pantalla las imagenes
    //que se encuentran contenidas en el mismo paquete dentro de un folder llamado imagenes
    //que contiene las fotos de cada uno de los autores del proyecto
    ImageIcon stefy = new ImageIcon(new ImageIcon(getClass().getResource("/imagenes/stefy.jpg")).getImage());
    ImageIcon karen = new ImageIcon(new ImageIcon(getClass().getResource("/imagenes/karen.jpg")).getImage());
    ImageIcon blanco = new ImageIcon(new ImageIcon(getClass().getResource("/imagenes/blanco.jpg")).getImage());
    ImageIcon danny = new ImageIcon(new ImageIcon(getClass().getResource("/imagenes/daniela.jpg")).getImage());
    ImageIcon jhon = new ImageIcon(new ImageIcon(getClass().getResource("/imagenes/john.jpg")).getImage());
   
    //Se crea un metodo por foto para posteriormente invocarlo en JFrame
    public void imagen1(Graphics g){
        g.drawImage(stefy.getImage(), 0, 0,250,250,null);
    }
    public void imagen2(Graphics g){
        g.drawImage(karen.getImage(),0, 0,260,260,null);
    }
    
    public void blanco(Graphics g){
        g.drawImage(blanco.getImage(),0, 0,260,260,null);
        
    }
    public void imagen3(Graphics g){
        g.drawImage(danny.getImage(),0, 0,260,260,null);
    }
    
    public void imagen4(Graphics g){
        g.drawImage(jhon.getImage(),0, 0,260,260,null);
    }
}
