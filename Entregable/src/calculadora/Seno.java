package calculadora;

public class Seno extends Operacion{
    
    double sen = 0;
       
    public Seno(double Co, double H) {
             
        super(Co, H, 'S');
        if(H==0) System.out.println("la altura no puede ser cero..");
        else this.sen = Co / H;
        this.setRes(this.sen);
    }
    
}