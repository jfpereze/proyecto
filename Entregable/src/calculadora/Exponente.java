package calculadora;

public class Exponente extends Operacion{
    
    double expo;
       
    public Exponente(double n1, double n2) {
             
        super(n1, n2, '^');
        this.expo = calcularPotencia(n1, n2);
        this.setRes(this.expo);
    }
    /**
     * 
     * @param n1 base
     * @param n2 exponente
     */
    
    public double calcularPotencia(double N, double p){
    	double result = N*N;
        p--;
        
        while(p!=1){
            result *= N;
            p--;
        }
        return result;
    }
}
