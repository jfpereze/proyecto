package calculadora;

public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double n1 = 2;
        double n2 = 3;
        double op = 3;
        
//        imprimir("Calculadora sencilla... Venvenido");
        
        //suma
        Exponente exp = new Exponente(n1,n2);
        exp.mostrarResultado();
        
        //seno
        
        Seno sen = new Seno(n1,n2);
        sen.mostrarResultado();
        
        //multiplicacion
        Multiplicacion mul = new Multiplicacion(n1,n2);
        mul.mostrarResultado();
        
        fibonacci fi = new fibonacci(n1,op);
        fi.mostrarResultado();
        
        //raiz
        Raiz ra = new Raiz(9, 3);
        ra.mostrarResultado();
        
    }

}
