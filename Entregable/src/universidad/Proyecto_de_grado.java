
package universidad;

public class Proyecto_de_grado extends Universidad{
    protected int puntosxlibros;
    protected int puntosxarticulos;
    protected int puntosxcapitulos;
    protected int puntosxsinregistrar;
    protected int totalpuntos;
    
    public Proyecto_de_grado(int cantidad_libros, int cantidad_articulos, int cantidad_capitulos, int cantidad_no_registrados){
        super(cantidad_libros, cantidad_articulos, cantidad_capitulos, cantidad_no_registrados);
        this.puntosxlibros=puntosxlibros;
        this.puntosxarticulos=puntosxarticulos;
        this.puntosxcapitulos=puntosxcapitulos;
        this.puntosxsinregistrar=puntosxsinregistrar;
        this.totalpuntos=totalpuntos;
        
    }
    
    public void leerdatos(){
        System.out.println("Cuantos libros utilizo para realizar este proyecto: ");
        cantidad_libros=lea.nextInt();
        System.out.println("Cuantos articulos utilizo para realizar este proyecto: ");
        cantidad_articulos=lea.nextInt();
        System.out.println("Cuantos capitulos de libros utilizo para realizar este proyecto: ");
        cantidad_capitulos=lea.nextInt();
        System.out.println("Cuantos productos sin registrar utilizo para realizar este proyecto: ");
        cantidad_no_registrados=lea.nextInt();
    }
    public void calcularpuntos(){
        puntosxlibros=cantidad_libros*10;
        System.out.println("\nTiene "+puntosxlibros+" puntos, por utilizar libros");
        puntosxarticulos=cantidad_articulos*20;
        System.out.println("\nTiene "+puntosxarticulos+" puntos, por utilizar articulos");
        puntosxcapitulos=cantidad_capitulos*5;
        System.out.println("\nTiene "+puntosxcapitulos+" puntos, por utilizar capitulos de libros");
        puntosxsinregistrar=cantidad_no_registrados*2;
        System.out.println("\nTiene "+puntosxsinregistrar+" puntos, por utilizar productos sin registar");
    }
    public void imprimirpuntos(){
        totalpuntos=(puntosxlibros)+(puntosxarticulos)+(puntosxcapitulos)+(puntosxsinregistrar);
        System.out.println("\nTiene en total: "+totalpuntos+" puntos");
    }
    public void mostar(){
        System.out.println("\nCantidad de Libros: "+cantidad_libros);
        System.out.println("\nCantidad de Articulos: "+cantidad_articulos);
    }
}
