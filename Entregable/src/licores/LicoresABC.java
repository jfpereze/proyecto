
package licores;
import java.util.Scanner;
public class LicoresABC {
     Scanner lea = new Scanner(System.in);
    static protected double costo;
    static protected double iva;
    static protected double ml;
    static protected double cantidad_cajas;
    static protected double cm3_total;
    static protected double totalencajas;
    static protected double cm3_litro;
    
    
   public LicoresABC(double costo, double iva, double ml, double cantidad_cajas, double cm3_total, double totalencajas, double cm3_litro){
        this.costo = costo;
        this.iva = iva;
        this.ml = ml;
        this.cantidad_cajas = cantidad_cajas;
        this.cm3_total = cm3_total;
        this.totalencajas=totalencajas;
    }
    public void leercostobotella(){
        System.out.print("Digite el precio de la botella: ");
        costo=lea.nextDouble();
    }
    public void leercostomedia(){
        System.out.print("Digite el precio de la media: ");
        costo=lea.nextDouble();
    }
    public void leercostocuarto(){
        System.out.print("Digite el precio del cuarto: ");
        costo=lea.nextDouble();
    }
   
    public void calculariva(){
        double porcentaje_iva;
        System.out.print("Digite valor del porcentaje del iva: ");
        porcentaje_iva=lea.nextDouble();
        iva=porcentaje_iva/100;
    }
    public void leerdatos(){
        System.out.print("Cuantos mililitros tiene el producto?: ");
        ml=lea.nextInt();
        System.out.print("Cuantas cajas se compraron de este producto?: ");
        cantidad_cajas=lea.nextInt();        
        
    }
   
    public void calcularcm3litro(){
        
        cm3_litro=(ml*1);
        totalencajas=(cantidad_cajas*6);
        cm3_total=(cm3_litro*totalencajas);
        
    }
    public void calcularcm3media(){
        
        cm3_litro=(ml*1);
        totalencajas=(cantidad_cajas*24);
        cm3_total=(cm3_litro*totalencajas);
    }
    public void calcularcm3cuartos(){
        
        cm3_litro=(ml*1);
        totalencajas=(cantidad_cajas*36);
        cm3_total=(cm3_litro*totalencajas);
    }
    public void imprimircalculos(){
        System.out.println("En total hay "+cm3_total+" cm3 en "+cantidad_cajas+" cajas de litro que se compraron");
    }
}
    

